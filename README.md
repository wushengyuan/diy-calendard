# 一个自定义的日历组件

## 效果图

![自定义日历效果图.gif](https://p1-juejin.byteimg.com/tos-cn-i-k3u1fbpfcp/8a16b9cda01f4081a81de2672fd67ae4~tplv-k3u1fbpfcp-watermark.image?)

## 运行

现在的vue3项目，基本上都需要node`16.19.0`版本以上才能运行起来，如果你的版本低于这个，请升级一下；

建议使用pnpm安装，比较快；
> npm i pnpm 

然后

> pnpm i

## 实现思路

详情思路请见：

[手把手教你如何封装一个自定义日历组件](https://juejin.cn/post/7215828320403324986)

## 代码

核心代码在component文件夹下的`DiyCalendar.vue`中。

## 字体

组件中使用到的字体，都是window系统内置的英文字体。但是如果你是macbook，有可能没有这些字体，没办法显示出来。

## 结语

如果帮到你了，请给我一个star。感激不尽~
